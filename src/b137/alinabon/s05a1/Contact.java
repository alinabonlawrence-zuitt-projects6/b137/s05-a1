package b137.alinabon.s05a1;

import java.util.ArrayList;

public class Contact {

    private String name;
    private ArrayList<String> numbers = new ArrayList<String>();
    private ArrayList<String> addresses = new ArrayList<String>();

    public Contact(){}

    public Contact(String name, String number, String  address){
        this.name = name;
        numbers.add(number);
        addresses.add(address);
    }

    public String getName(){
        return name;
    }

    public void getAddresses() {
        for(String address: addresses){
            System.out.println(address);
        }
    }

    public void getNumbers() {
        for(String number: numbers){
            System.out.println(number);
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddresses(String address) {
        addresses.add(address);
    }

    public void setNumbers(String number) {
        numbers.add(number);
    }
}
