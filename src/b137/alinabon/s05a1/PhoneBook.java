package b137.alinabon.s05a1;

import java.util.ArrayList;

public class PhoneBook{

    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    public PhoneBook(){}

    public PhoneBook(Contact contact){
        contacts.add(contact);
    }

    public ArrayList<Contact> getContacts(){
        return contacts;
    }

    public void setContact(Contact contact) {
        contacts.add(contact);
    }

    public void showPhoneBook(){
        if (contacts.size() == 0){
            System.out.println("Phonebook is Empty");
            System.out.println();
        }else{
            for (Contact contact : contacts) {
                System.out.println(contact.getName());
                System.out.println("--------------------------------------------------------------");
                System.out.println( contact.getName() + " has the following registered numbers: ");
                contact.getNumbers();
                System.out.println("--------------------------------------------------------------");
                System.out.println( contact.getName() + " has the following registered addresses: ");
                contact.getAddresses();
                System.out.println("==============================================================");
            }
        }
    }


}
