package b137.alinabon.s05a1;

public class Main {

    public static void main(String[] args){

        PhoneBook phoneBook = new PhoneBook();

        phoneBook.showPhoneBook();

        Contact contact = new Contact();
        contact.setName("Jane Doe");
        contact.setNumbers("09125436987");
        contact.setNumbers("09123665478");
        contact.setAddresses("my Home is in Cebu City");
        contact.setAddresses("my Office is in Cebu City");

        Contact contact2 = new Contact();
        contact2.setName("John Doe");
        contact2.setNumbers("09569874563");
        contact2.setNumbers("09123665478");
        contact2.setAddresses("my Home is in Cebu City");
        contact2.setAddresses("my Office is in Cebu City");


        phoneBook.setContact(contact);
        phoneBook.setContact(contact2);
        phoneBook.showPhoneBook();

    }
}
